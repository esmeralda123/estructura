
package listasdobles;

import java.util.Objects;

/**
 *
 * @author Emirly
 */
public class Cancion {
    
    private String nombre;
    private String artista;
    private String album;
    
    public Cancion( String n, String a, String al){
        this.nombre=n;
        this.album=al;
        this.artista=a;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the artista
     */
    public String getArtista() {
        return artista;
    }

    /**
     * @param artista the artista to set
     */
    public void setArtista(String artista) {
        this.artista = artista;
    }

    /**
     * @return the album
     */
    public String getAlbum() {
        return album;
    }

    /**
     * @param album the album to set
     */
    public void setAlbum(String album) {
        this.album = album;
    }
    @Override
    public String toString(){
    return nombre+" - "+artista+" - "+album;
    }
    
    @Override
    public boolean equals(Object other){
        Cancion cancion =(Cancion)other;
        if(cancion.getArtista().equals(this.artista)){
        cancion.getNombre().equals(this.nombre);}
        return true;
                
               
        
    }

    
}


