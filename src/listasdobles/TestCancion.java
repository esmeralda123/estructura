
package listasdobles;

public class TestCancion {
    public static void main(String... args){
        int n=5;
        Cancion cancion[]=new Cancion[n];
        cancion[0]= new Cancion("pajarillo", "napoleon","20 grandes Exitos");
        cancion[1]= new Cancion("ella se llamaba martha", "napoleon","20 grandes Exitos");
        cancion[2]= new Cancion("vive", "napoleon","20 grandes Exitos");
        cancion[3]= new Cancion("roots", "imagine dragons","smoke+mirrors");
        cancion[4]= new Cancion("dream", "imagine dragons","smoke+mirrors");
        
        
        DLLCancion canciones = new DLLCancion();
        canciones.addToHead(cancion[0]);
        canciones.addToHead(cancion[3]);
        canciones.addToHead(cancion[4]);
        canciones.addToTail(cancion[2]);
        canciones.addToHead(cancion[1]);
        //canciones.printAll();
        
        
        //System.out.println(canciones.buscarCancionPorNombre("Ella se llamaba martha"));
        System.out.println(canciones.buscarCancionPorInterprete("imagine dragons"));
        
    }
}
