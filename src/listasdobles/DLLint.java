
package listasdobles;

/**
 *
 * @author Emirly
 */
public class DLLint {
    
    NodeDLLint head;
    NodeDLLint tail;
    
    public DLLint(){
        head= tail=null;
    }
    
    public boolean isEmpty(){
        return head==null;
    }
    
    
    public void addToHead(int el ){
        if(!isEmpty()){
           head=new NodeDLLint (el,null,head);
           head.next.prev=head;
            
        }else{
            head= tail=new NodeDLLint(el);
        }
            
    }
    
    public void addToTail(int el){
        if(!isEmpty()){
            tail= new NodeDLLint(el,tail,null);
            tail.prev.next=tail;
        }else{
            head=tail = new NodeDLLint(el);
        }
        
    }
    
    public void printAll(){
        NodeDLLint tmp;
        
        for(tmp=head;tmp!=null;tmp=tmp.next){
            System.out.print(tmp.info+"-");
        }
    }
    
}
