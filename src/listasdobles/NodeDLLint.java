
package listasdobles;

public class NodeDLLint {
    
    int info;
    NodeDLLint next;
    NodeDLLint prev;
    
    public NodeDLLint(int e,NodeDLLint p,NodeDLLint n ){
        this.info=e;
        this.prev=p;
        this.next=n;
        
    }
    
    
     public NodeDLLint(int el ){
        this(el,null,null);
        
    }
}
