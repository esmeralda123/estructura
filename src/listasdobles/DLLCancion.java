/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasdobles;

/**
 *
 * @author Emirly
 */
public class DLLCancion {
     NodoCancionDLL head;
    NodoCancionDLL tail;
    
    public DLLCancion(){
        head = tail = null;
    }
    
    public boolean isEmpty(){
        return head == null;
    }
    
    public void addToHead(Cancion el){
        if(!isEmpty()){
            head = new NodoCancionDLL(el,null,head);
            head.next.prev = head;
        }else{
            head = tail = new NodoCancionDLL(el);
        }
    }
    
    public void addToTail(Cancion el){
        if(!isEmpty()){
            tail = new NodoCancionDLL(el,tail,null);
            tail.prev.next = tail;
        }else{
            head = tail = new NodoCancionDLL(el);
        }
    }
    
    public void printAll(){
        NodoCancionDLL tmp;
        for(tmp=head; tmp != null ; tmp=tmp.next){
            System.out.println(" - "+tmp.info);
        }
    }
    
    public String buscarCancionPorNombre(String nombre){
        String res="no encontrado";
        NodoCancionDLL tmp;
        for(tmp=head; tmp != null ; tmp=tmp.next){
            
            if(tmp.info.getNombre().equals(nombre)){
                res=tmp.info.toString();
            } 
        }
       return res; 
        
    }
    
    public String buscarCancionPorInterprete(String artista){
        String res="";
        String n="";
        
        
        NodoCancionDLL tmp;
        
        for(tmp=head; tmp != null ; tmp=tmp.next){
            
            if(tmp.info.getArtista().equals(artista)){
                res=tmp.info.toString();
                n+=res+"\n";
                
           }
            
        }
       return n;
    }
    

    
}
