/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasdobles;

/**
 *
 * @author Emirly
 */
public class NodoCancionDLL {
    
    Cancion info;
    NodoCancionDLL next;
    NodoCancionDLL prev;
    
    public NodoCancionDLL(Cancion e,NodoCancionDLL p,NodoCancionDLL n ){
        this.info=e;
        this.prev=p;
        this.next=n;
        
    }
    
    
     public NodoCancionDLL(Cancion el ){
        this(el,null,null);
        
    }
    
}
